#_**Everything**_ starts or is eventually transformed into text. 

With the increase in technology, all of the words we speak are being recorded. Lectures are quickly written by students, verbal conversations between people are publicly tweeted online, and people no longer use phones to call, but to send text messages. 

Steven Shaviro says **“No matter what words you utter, those words will have been anticipated somewhere in the chains of discourse”** (p. 5).

People often say that once you put something on the internet, it stays there forever. What Shaviro shows is that this is true of all places, not limited to the internet, _“There is no place of indemnity that would somehow be free of these constraints”_ (p. 5). 

He focuses on video surveillance, describing how video surveillance systems are created to record everything, endless footage of absolutely nothing in order to capture a few seconds of action; this system is used as a preventative measure. 

_However, text is monitored just as much as video, if not more._ Shaviro says that video footage is an archive “most of which nobody ever looks at” (p.36). The same is true for text. Social media and the internet has made it incredibly easy to record anything written or typed. We are all attached to it, and we all have access to it.

![Alt text](https://i0.wp.com/personalinnovationhub.com/wp-content/uploads/2014/06/social_listening-service.jpg?fit=650%2C436)

All text monitoring, like video surveillance, is used to prevent possible danger. This means that everything we transform into text, every thought, joke, and idea, is seen by people not necessarily intended to see it.

Instagram post are flagged for inappropriate content, tweets are questioned when they seem sketchy or dangerous, text messages are not private between you and your friend, but also the NSA. 

![Alt text](https://www.sonus.net/sites/default/files/sonus_sbc_nectar.jpg)

“Instead of a single, self-contained Text that would comprehend the entire world within its pages, we face an infinitude of images, flickering on an infinitude of screens, each presenting its own singular point of view” (p. 36).  Now in the world, those images and screens include text. We have transformed text _beyond the definition it held before._

We no longer use our own words to express ourselves, but images with outlining text.

![Alt text](http://weknowmemes.com/wp-content/uploads/2014/04/tyrone-biggums-meme.jpg)

“Genes and memes are helpless without their hosts,” (p.16) and we have voluntarily invited memes to live inside of us, completely changing how we communicate with each other. 

**The self-contained text that would allow us to comprehend the entire world, no matter how we decide to perceive it, is the internet.** We use text to develop the internet, by creating software and coding. We use text to fill the internet with information and content. And we use text to explore the internet. 

##_Text has created our world and text has allowed us to keep our world alive._ 



